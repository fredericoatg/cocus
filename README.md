Web Components - Country Autocomplete, Date Picker, and Integration

## Description

This project consists of three folders, each containing a web component that serves a specific purpose. The first folder contains a country autocomplete input field component that provides suggestions for countries as the user types. The second folder contains a reusable date picker component. The third folder integrates the country autocomplete and date picker components to display the selected country and date together.

## Folder Structure

- `exercise-1`: Contains the country autocomplete input field component.
- `exercise-2`: Contains the reusable date picker component.
- `exercise-3`: Contains the integration component that combines the country autocomplete and date picker components.

## Technologies Used

- HTML: Markup language used to define the structure of web components and their templates.
- CSS: Styling language used to enhance the appearance of web components.
- JavaScript: Programming language used for component logic and interactivity.


