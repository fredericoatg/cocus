class AutocompleteInput extends HTMLElement {
  constructor() {
    super();

    // Create a shadow root and add the initial content
    this.attachShadow({ mode: "open" });

    // Set the innerHTML with the styles and structure for the autocomplete component
    this.shadowRoot.innerHTML = `
            <div class="autocomplete">
                <input id="autocomplete-input" type="text" />
                <button id="clear-icon" class="clear-icon">X</button>
                <div id="autocomplete-items" class="autocomplete-items"></div>
            </div>
        `;

    // Fetch and apply the external styles
    this.applyStyles();
  }

  connectedCallback() {
    // Get the necessary elements from the shadow root
    this.inputElement = this.shadowRoot.querySelector("#autocomplete-input");
    this.items = this.shadowRoot.querySelector("#autocomplete-items");
    this.clearIcon = this.shadowRoot.querySelector("#clear-icon");

    // Add event listeners for input, item click, and clear icon click events
    this.inputElement.addEventListener("input", this.handleInput.bind(this));
    this.items.addEventListener(
      "click",
      this.handleItemClick.bind(this)
    );
    this.clearIcon.addEventListener(
      "click",
      this.handleClearIconClick.bind(this)
    );
  }

  async applyStyles() {
    const response = await fetch("./autocomplete-input.css");
    const styles = await response.text();
    const styleElement = document.createElement("style");
    styleElement.textContent = styles;
    this.shadowRoot.appendChild(styleElement);
  }

  // Handle the input event
  async handleInput(event) {
    const keyword = event.target.value.trim();

    // Clear the timer if it exists
    if (this.timer) {
      clearTimeout(this.timer);
    }

    // Clear items if the input is empty
    if (keyword.length < 1) {
      this.clearItems();
      return;
    }

    // Defer the API call for 200ms to avoid unnecessary load
    this.timer = setTimeout(async () => {
      const url = `https://api.cloud.tui.com/search-destination/v2/de/package/TUICOM/2/autosuggest/peakwork/${keyword}`;
      const response = await fetch(url);
      const data = await response.json();

      // Render the items if data is available
      if (data && data.length > 0 && data[0].items) {
        this.renderItems(data[0].items);
      } else {
        this.clearItems();
      }
    }, 200);
  }

  // Render the items in the autocomplete dropdown
  renderItems(items) {
    this.clearItems();

    for (const item of items) {
      const div = document.createElement("div");
      div.classList.add("autocomplete-item");
      div.textContent = item.name;
      this.items.appendChild(div);
    }
  }

  // Handle item click event
  handleItemClick(event) {
    if (event.target.classList.contains("autocomplete-item")) {
      this.inputElement.value = event.target.textContent;

      // Emit a custom event with the selected item's data
      const selectedItemEvent = new CustomEvent("autocomplete-select", {
        detail: {
          name: event.target.textContent,
        },
        bubbles: true,
        composed: true,
      });
      this.dispatchEvent(selectedItemEvent);

      this.clearItems();
    }
  }

  // Handle clear icon click event
  handleClearIconClick() {
    this.inputElement.value = "";
    this.clearItems();
  }

  // Clear the items in the autocomplete dropdown
  clearItems() {
    while (this.items.firstChild) {
      this.items.removeChild(this.items.firstChild);
    }
  }
}

customElements.define("autocomplete-input", AutocompleteInput);
