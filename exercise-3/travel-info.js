class TravelInfo extends HTMLElement {
  constructor() {
    super();

    // Create a shadow root and add the initial content
    this.attachShadow({ mode: "open" });

    // Set the innerHTML with the structure for the travel summary component
    this.shadowRoot.innerHTML = `
          <style>
              /* Add any styles you want for the travel summary component */
          </style>
          <div>
              <p>Selected destination: <span id="selected-destination">-</span></p>
              <p>Selected travel date: <span id="selected-date">-</span></p>
          </div>
      `;

    this.selectedDestinationElement = this.shadowRoot.querySelector(
      "#selected-destination"
    );
    this.selectedDateElement = this.shadowRoot.querySelector("#selected-date");
  }

  updateDestination(destination) {
    this.selectedDestinationElement.textContent = destination;
  }

  updateDate(date) {
    const formattedDate = this.formatDate(date);
    this.selectedDateElement.textContent = formattedDate;
  }

  formatDate(date) {
    const [year, month, day] = date.split("-");
    return `${day}.${month}.${year}`;
  }
}

customElements.define("travel-info", TravelInfo);
