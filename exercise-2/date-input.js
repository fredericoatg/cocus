class DateInput extends HTMLElement {
  constructor() {
    super();

    // Attach a shadow DOM to the custom element
    this.attachShadow({ mode: "open" });

    // Add the input element and styles to the shadow DOM
    this.shadowRoot.innerHTML = `
            <style>
                input {
                    font-size: 1em;
                    padding: 0.25em;
                    border: 1px solid #ccc;
                }
            </style>
            <input type="date" />
        `;

    // Get a reference to the input element
    this.inputElement = this.shadowRoot.querySelector("input");
    // Add a change event listener to the input element
    this.inputElement.addEventListener(
      "change",
      this.handleDateChange.bind(this)
    );
  }

  // Define the event handler function for the change event
  handleDateChange(e) {
    // Get the chosen date from the input element
    const date = e.target.value;
    // Create a custom event with the chosen date
    const dateChangeEvent = new CustomEvent("dateChange", {
      detail: { date },
      bubbles: true,
      composed: true,
    });
    // Dispatch the custom event from the custom element
    this.dispatchEvent(dateChangeEvent);
  }
}

// Register the custom element with the browser
customElements.define("date-input", DateInput);
